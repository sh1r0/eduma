package teacher

import "github.com/jinzhu/gorm"

type Teacher struct {
	gorm.Model
	FirstName    string
	LastName     string
	Email        string `gorm:"type:varchar(100);not null;unique_index"`
	PasswordHash string
	Bio          string
	Gender       string
}

type Request struct {
	Teacherid string
	FirstName string
	LastName  string
	Email     string
	Password  string
	Teacher   Teacher
	Bio       string
}

type Response struct {
	Id      uint
	Teacher *Teacher
}
