package teacher

import "github.com/jinzhu/gorm"

func FindTeacherByEmail(db *gorm.DB, email string) (*Teacher, error) {
	var teacher Teacher
	res := db.Find(&teacher, &Teacher{Email: email})
	if res.RecordNotFound() {
		return nil, &EmailNotExistsError{}
	}
	return &teacher, nil
}

func GetAllTeachers(db *gorm.DB) []*Teacher {
	var teachers []*Teacher
	_ = db.Find(&teachers)
	return teachers
}
