package teacher

import (
	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/bcrypt"
)

func Login(db *gorm.DB, req *Request) (*Response, error) {
	teacher, err := FindTeacherByEmail(db, req.Teacherid)
	if err != nil {
		return nil, err
	}

	err = bcrypt.CompareHashAndPassword([]byte(teacher.PasswordHash), []byte(req.Password))
	if err != nil {
		return nil, &PasswordMismatchError{}
	}
	return &Response{Teacher: teacher}, nil
}

func Signup(db *gorm.DB, req *Request) (*Response, error) {
	passwordHash, err := bcrypt.GenerateFromPassword([]byte(req.Password), bcrypt.DefaultCost)
	if err != nil {
		return nil, err
	}
	newTeacher := &Teacher{
		FirstName:    req.FirstName,
		LastName:     req.LastName,
		Email:        req.Email,
		PasswordHash: string(passwordHash),
	}

	id, err := Create(db, newTeacher)
	if err != nil {
		return nil, err
	}
	return &Response{Id: id}, err
}
