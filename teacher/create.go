package teacher

import (
	"eduman/postgres"

	"github.com/jinzhu/gorm"
)

func Create(db *gorm.DB, teacher *Teacher) (uint, error) {
	err := db.Create(teacher).Error
	if err != nil {
		if postgres.IsUniqueConstraintError(err, UniqueConstraintEmail) {
			return 0, &EmailDuplicateError{Email: teacher.Email}
		}
		return 0, err
	}
	return teacher.ID, nil
}
