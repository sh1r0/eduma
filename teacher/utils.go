package teacher

import "fmt"

const (
	UniqueConstraintEmail = "teachers_email_key"
)

type TeacherNotExistsError struct{}
type EmailNotExistsError struct{}
type EmailDuplicateError struct {
	Email string
}

func (*TeacherNotExistsError) Error() string {
	return "Teacher does not exist!"
}

func (*EmailNotExistsError) Error() string {
	return "email does not exist!"
}

func (e *EmailDuplicateError) Error() string {
	return fmt.Sprintf("Email '%s' already exists", e.Email)
}

type PasswordMismatchError struct{}

func (e *PasswordMismatchError) Error() string {
	return "password didn't match"
}
