package web

import (
	"eduman/student"
	"eduman/utils"
	"fmt"
	"net/http"
	"time"
)

func ParentsHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println(time.Now().Format("2006/01/02 15:04:05") + " " + r.URL.Path)
	// Post handles the parent's subscription request
	parentid, err := utils.GetUserID(r)

	if err != nil {
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}

	config, _ := utils.LoadConfig("config.json")
	db, err := utils.DBCon(config)
	utils.PanicOnError(err)
	defer db.Close()

	usr, err := student.FindParentByEmail(db, parentid)

	if err != nil {
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}

	if r.Method == "GET" {
		// requires login to access
		tpl := "templates/parents/home.tmpl"

		args := struct {
			Title  string
			Parent *student.Parent
		}{
			Title:  "Parents Dashboard | EduMan",
			Parent: usr,
		}

		utils.GenericTemplate(w, r, tpl, args, "parents")
	}
}

func ParentsLogin(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		err := r.ParseForm()
		if err != nil {
			fmt.Println(fmt.Errorf("Error: %v", err))
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		parentid := r.Form.Get("parentid")
		password := r.Form.Get("password")

		config, _ := utils.LoadConfig("config.json")
		db, err := utils.DBCon(config)
		utils.PanicOnError(err)
		defer db.Close()

		// parent login
		_, err = student.Login(db, &student.Request{
			Parentid: parentid,
			Password: password,
		})
		if err != nil {
			fmt.Printf("Login error '%s' for %s \n", err.Error(), parentid)
			http.Redirect(w, r, "/", http.StatusFound)
			return
		}

		utils.ClearSession(w)
		utils.SetSession(parentid, w)
		http.Redirect(w, r, "/parents", http.StatusFound)
	}
}
