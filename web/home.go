package web

import (
	"eduman/utils"
	"fmt"
	"net/http"
	"time"
)

func Home(w http.ResponseWriter, r *http.Request) {
	fmt.Println(time.Now().Format("2006/01/02 15:04:05") + " " + r.URL.Path)

	/*config, _ := utils.LoadConfig("config.json")
	db, err := utils.DBCon(config)
	utils.PanicOnError(err)
	defer db.Close()*/

	if r.Method == "GET" {
		tpl := "templates/home.tmpl"

		args := struct {
			Title string
		}{
			Title: "EduMan - Student Management System",
		}

		utils.TemplateHandler(w, r, tpl, args)
	}
}

func LogoutHandler(w http.ResponseWriter, r *http.Request) {
	utils.ClearSession(w)
	// fmt.Println("Session cleared!")
	http.Redirect(w, r, "/", http.StatusFound)
}
