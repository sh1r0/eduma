package web

import (
	"eduman/utils"
	"net/http"
	"path"
)

// Serving files
func ServeFileHandler(res http.ResponseWriter, req *http.Request) {
	fname := path.Base(req.URL.Path)
	filePath := "assets/files/" + fname
	http.ServeFile(res, req, utils.AppFilePath(filePath))
}

func GenericHandler(w http.ResponseWriter, r *http.Request, tpl string, title string) {
	args := utils.Context{
		Title: title,
	}
	utils.TemplateHandler(w, r, tpl, args)
}

func ConsultHandler(w http.ResponseWriter, r *http.Request) {
	title := "AI Consultancy | MegaCog"
	tpl := "templates/consult.tmpl"
	GenericHandler(w, r, tpl, title)
}

func AboutHandler(w http.ResponseWriter, r *http.Request) {
	title := "About | MegaCog"
	tpl := "templates/about.tmpl"
	GenericHandler(w, r, tpl, title)
}

func TermsHandler(w http.ResponseWriter, r *http.Request) {
	title := "Terms of Use | MegaCog"
	tpl := "templates/terms.tmpl"
	GenericHandler(w, r, tpl, title)
}

func FAQHandler(w http.ResponseWriter, r *http.Request) {
	title := "FAQs | MegaCog"
	tpl := "templates/faqs.tmpl"
	GenericHandler(w, r, tpl, title)
}

func PolicyHandler(w http.ResponseWriter, r *http.Request) {
	title := "Privacy Policy | MegaCog"
	tpl := "templates/privacy.tmpl"
	GenericHandler(w, r, tpl, title)
}

/*
func TipsHandler(w http.ResponseWriter, r *http.Request) {
	title := "Tips and Tricks"
	tpl := "templates/tips.tmpl"
	GenericHandler(w, r, tpl, title)
}

func PricingHandler(w http.ResponseWriter, r *http.Request) {
	title := "Pricing"
	tpl := "templates/pricing.tmpl"
	GenericHandler(w, r, tpl, title)
}
*/
