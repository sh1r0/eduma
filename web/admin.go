package web

import (
	"eduman/student"
	"eduman/teacher"
	"eduman/utils"
	"fmt"
	"net/http"
)

func AdminHome(w http.ResponseWriter, r *http.Request) {
	admin, err := utils.GetUserID(r)

	if err != nil {
		http.Redirect(w, r, "/admin/login", http.StatusFound)
		return
	} else if admin != "admin" {
		http.Redirect(w, r, "/admin/login", http.StatusFound)
		return
	}

	if r.Method == "GET" {
		tpl := "templates/admin/home.tmpl"

		args := struct {
			Title string
			Name  string
		}{
			Title: "Admin Dashboard | EduMan",
			Name:  admin,
		}

		utils.GenericTemplate(w, r, tpl, args, "admin")
	}
}

func AdminSettings(w http.ResponseWriter, r *http.Request) {
	admin, err := utils.GetUserID(r)

	if err != nil {
		http.Redirect(w, r, "/admin/login", http.StatusFound)
		return
	} else if admin != "admin" {
		http.Redirect(w, r, "/admin/login", http.StatusFound)
		return
	}

	if r.Method == "GET" {
		tpl := "templates/admin/settings.tmpl"

		args := struct {
			Title string
			Name  string
		}{
			Title: "Admin Settings | EduMan",
			Name:  admin,
		}

		utils.GenericTemplate(w, r, tpl, args, "admin")
	}
}

func AdminTeachers(w http.ResponseWriter, r *http.Request) {
	admin, err := utils.GetUserID(r)

	if err != nil {
		http.Redirect(w, r, "/admin/login", http.StatusFound)
		return
	} else if admin != "admin" {
		http.Redirect(w, r, "/admin/login", http.StatusFound)
		return
	}

	if r.Method == "GET" {
		tpl := "templates/admin/teachers.tmpl"

		config, _ := utils.LoadConfig("config.json")
		db, err := utils.DBCon(config)
		utils.PanicOnError(err)
		defer db.Close()

		teachers := teacher.GetAllTeachers(db)

		args := struct {
			Title    string
			Name     string
			Teachers []*teacher.Teacher
		}{
			Title:    "Admin Teachers | EduMan",
			Name:     admin,
			Teachers: teachers,
		}

		utils.GenericTemplate(w, r, tpl, args, "admin")
	}
}

func AdminParents(w http.ResponseWriter, r *http.Request) {
	admin, err := utils.GetUserID(r)

	if err != nil {
		http.Redirect(w, r, "/admin/login", http.StatusFound)
		return
	} else if admin != "admin" {
		http.Redirect(w, r, "/admin/login", http.StatusFound)
		return
	}

	if r.Method == "GET" {
		tpl := "templates/admin/parents.tmpl"

		config, _ := utils.LoadConfig("config.json")
		db, err := utils.DBCon(config)
		utils.PanicOnError(err)
		defer db.Close()

		parents := student.GetAllParents(db)

		args := struct {
			Title   string
			Name    string
			Parents []*student.Parent
		}{
			Title:   "Admin Teachers | EduMan",
			Name:    admin,
			Parents: parents,
		}

		utils.GenericTemplate(w, r, tpl, args, "admin")
	}
}

func AdminLogin(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		tpl := "templates/admin/login.tmpl"

		args := struct {
			Title string
		}{
			Title: "Admin Login | EduMan",
		}

		utils.GenericTemplate(w, r, tpl, args, "layout")
	} else if r.Method == "POST" {
		err := r.ParseForm()
		if err != nil {
			fmt.Println(fmt.Errorf("Error: %v", err))
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		adminid := r.Form.Get("adminid")
		password := r.Form.Get("password")

		if adminid == "admin" && password == "eduman" {
			utils.SetSession(adminid, w)
			http.Redirect(w, r, "/admin", http.StatusFound)
			return
		} else {
			http.Redirect(w, r, "/admin/login", http.StatusFound)
			return
		}
	}
}

func CreateTeacher(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		err := r.ParseForm()
		if err != nil {
			fmt.Println(fmt.Errorf("Error: %v", err))
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		fname := r.Form.Get("fname")
		lname := r.Form.Get("lname")
		email := r.Form.Get("email")
		password := r.Form.Get("password")

		config, _ := utils.LoadConfig("config.json")
		db, err := utils.DBCon(config)
		utils.PanicOnError(err)
		defer db.Close()

		// teacher login
		_, err = teacher.Signup(db, &teacher.Request{
			FirstName: fname,
			LastName:  lname,
			Email:     email,
			Password:  password,
		})
		if err != nil {
			http.Redirect(w, r, "/admin", http.StatusFound)
			return
		}

		http.Redirect(w, r, "/admin/teachers", http.StatusFound)
	}
}

func CreateParent(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		err := r.ParseForm()
		if err != nil {
			fmt.Println(fmt.Errorf("Error: %v", err))
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		fname := r.Form.Get("fname")
		lname := r.Form.Get("lname")
		email := r.Form.Get("email")
		password := r.Form.Get("password")

		config, _ := utils.LoadConfig("config.json")
		db, err := utils.DBCon(config)
		utils.PanicOnError(err)
		defer db.Close()

		// teacher login
		_, err = student.RegisterParent(db, &student.Request{
			FirstName: fname,
			LastName:  lname,
			Email:     email,
			Password:  password,
		})
		if err != nil {
			http.Redirect(w, r, "/admin", http.StatusFound)
			return
		}

		http.Redirect(w, r, "/admin/parents", http.StatusFound)
	}
}
