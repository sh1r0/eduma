package web

import (
	"eduman/student"
	"eduman/utils"
	"fmt"
	"net/http"
)

func RegisterStudent(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		err := r.ParseForm()
		if err != nil {
			fmt.Println(fmt.Errorf("Error: %v", err))
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		fname := r.Form.Get("fname")
		lname := r.Form.Get("lname")
		suid := r.Form.Get("suid")

		config, _ := utils.LoadConfig("config.json")
		db, err := utils.DBCon(config)
		utils.PanicOnError(err)
		defer db.Close()

		// teacher login
		_, err = student.RegisterStudent(db, &student.Request{
			FirstName: fname,
			LastName:  lname,
			StudentID: suid,
		})
		http.Redirect(w, r, "/teachers", http.StatusFound)
	}
}
