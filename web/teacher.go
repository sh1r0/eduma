package web

import (
	"eduman/teacher"
	"eduman/utils"
	"fmt"
	"net/http"
	"time"
)

func TeacherHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println(time.Now().Format("2006/01/02 15:04:05") + " " + r.URL.Path)
	// Post handles the teacher's subscription request
	teacherid, err := utils.GetUserID(r)

	if err != nil {
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}

	config, _ := utils.LoadConfig("config.json")
	db, err := utils.DBCon(config)
	utils.PanicOnError(err)
	defer db.Close()

	usr, err := teacher.FindTeacherByEmail(db, teacherid)

	if err != nil {
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}

	if r.Method == "GET" {
		// requires login to access
		tpl := "templates/teachers/home.tmpl"

		args := struct {
			Title   string
			Teacher *teacher.Teacher
		}{
			Title:   "Teachers Portal | EduMan",
			Teacher: usr,
		}

		utils.GenericTemplate(w, r, tpl, args, "teachers")
	}
}

func TeacherLogin(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		err := r.ParseForm()
		if err != nil {
			fmt.Println(fmt.Errorf("Error: %v", err))
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		teacherid := r.Form.Get("teacherid")
		password := r.Form.Get("password")

		config, _ := utils.LoadConfig("config.json")
		db, err := utils.DBCon(config)
		utils.PanicOnError(err)
		defer db.Close()

		// teacher login
		_, err = teacher.Login(db, &teacher.Request{
			Teacherid: teacherid,
			Password:  password,
		})
		if err != nil {
			fmt.Printf("Login error '%s' for %s \n", err.Error(), teacherid)
			http.Redirect(w, r, "/", http.StatusFound)
			return
		}

		utils.ClearSession(w)
		utils.SetSession(teacherid, w)
		http.Redirect(w, r, "/teachers", http.StatusFound)
	}
}
