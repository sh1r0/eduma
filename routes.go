package main

import (
	"eduman/utils"
	"eduman/web"
	"net/http"

	"github.com/gorilla/mux"
)

func router() *mux.Router {
	r := mux.NewRouter()

	// serving static files
	staticFileDirectory := http.Dir(utils.AppFilePath("assets/"))

	staticFileHandler := http.StripPrefix("/assets/",
		http.FileServer(staticFileDirectory))
	r.PathPrefix("/assets/").Handler(staticFileHandler).Methods("GET")

	// serve a file
	r.HandleFunc("/favicon.ico", web.ServeFileHandler)
	r.HandleFunc("/robots.txt", web.ServeFileHandler)
	r.HandleFunc("/ads.txt", web.ServeFileHandler)

	// Web page routes
	r.HandleFunc("/", web.Home).Methods("GET")
	r.HandleFunc("/logout", web.LogoutHandler).Methods("GET")

	// Parents routes
	r.HandleFunc("/teachers", web.TeacherHandler).Methods("GET")
	r.HandleFunc("/teacher/login", web.TeacherLogin).Methods("POST")

	r.HandleFunc("/parents", web.ParentsHandler).Methods("GET")
	r.HandleFunc("/parent/login", web.ParentsLogin).Methods("POST")

	//
	r.HandleFunc("/admin", web.AdminHome).Methods("GET")
	r.HandleFunc("/admin/login", web.AdminLogin).Methods("GET", "POST")
	r.HandleFunc("/admin/settings", web.AdminSettings).Methods("GET")
	r.HandleFunc("/admin/teachers", web.AdminTeachers).Methods("GET")
	r.HandleFunc("/admin/parents", web.AdminParents).Methods("GET")
	r.HandleFunc("/admin/addteacher", web.CreateTeacher).Methods("POST")
	r.HandleFunc("/admin/addparent", web.CreateParent).Methods("POST")

	return r
}
