package student

import "fmt"

const (
	UniqueConstraintEmail = "parents_email_key"
)

type ParentNotExistsError struct{}
type EmailNotExistsError struct{}
type StudentNotExistsError struct{}
type EmailDuplicateError struct {
	Email string
}

func (*ParentNotExistsError) Error() string {
	return "Parent does not exist!"
}

func (*StudentNotExistsError) Error() string {
	return "Student does not exist!"
}

func (*EmailNotExistsError) Error() string {
	return "email does not exist!"
}

func (e *EmailDuplicateError) Error() string {
	return fmt.Sprintf("Email '%s' already exists", e.Email)
}

type PasswordMismatchError struct{}

func (e *PasswordMismatchError) Error() string {
	return "password didn't match"
}
