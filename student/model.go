package student

import "github.com/jinzhu/gorm"

type Student struct {
	gorm.Model
	FirstName  string
	LastName   string
	Studentuid string `gorm:"type:varchar(10);not null;unique_index"`
}

type Class struct {
	gorm.Model
	Name string
}

type Record struct {
	gorm.Model
	StudentID  uint
	Student    Student
	Attendance uint
	ClassID    uint
	Class      Class
	Grade      string
}

type Medical struct {
	gorm.Model
	DateOfBirth       string
	Gender            string
	ExistingCondition string
	Clinics           []*Clinic
}

type Clinic struct {
	gorm.Model
	ClinicName string
	Diagnosis  string
	Treatment  string
}

type Billing struct {
	gorm.Model
	StudentID uint
	Student   Student
	Balance   float32
	Payment   float32
	Details   string
}

type Parent struct {
	gorm.Model
	FirstName    string
	LastName     string
	Email        string `gorm:"type:varchar(100);not null;unique_index"`
	PasswordHash string
	Students     []*Student
	Address      string
}

type Request struct {
	Parentid  string
	FirstName string
	LastName  string
	Email     string
	Password  string
	Parent    Parent
	Student   Student
	StudentID string
}

type Response struct {
	Id      uint
	Parent  *Parent
	Student *Student
}
