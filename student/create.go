package student

import (
	"eduman/postgres"

	"github.com/jinzhu/gorm"
)

func Create(db *gorm.DB, parent *Parent) (uint, error) {
	err := db.Create(parent).Error
	if err != nil {
		if postgres.IsUniqueConstraintError(err, UniqueConstraintEmail) {
			return 0, &EmailDuplicateError{Email: parent.Email}
		}
		return 0, err
	}
	return parent.ID, nil
}

func CreateStudent(db *gorm.DB, student *Student) (uint, error) {
	err := db.Create(student).Error
	if err != nil {
		return 0, err
	}
	return student.ID, nil
}
