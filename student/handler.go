package student

import (
	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/bcrypt"
)

func Login(db *gorm.DB, req *Request) (*Response, error) {
	parent, err := FindParentByEmail(db, req.Parentid)
	if err != nil {
		return nil, err
	}

	err = bcrypt.CompareHashAndPassword([]byte(parent.PasswordHash), []byte(req.Password))
	if err != nil {
		return nil, &PasswordMismatchError{}
	}
	return &Response{Parent: parent}, nil
}

func RegisterParent(db *gorm.DB, req *Request) (*Response, error) {
	passwordHash, err := bcrypt.GenerateFromPassword([]byte(req.Password), bcrypt.DefaultCost)
	if err != nil {
		return nil, err
	}
	newParent := &Parent{
		FirstName:    req.FirstName,
		LastName:     req.LastName,
		Email:        req.Email,
		PasswordHash: string(passwordHash),
	}

	id, err := Create(db, newParent)
	if err != nil {
		return nil, err
	}
	return &Response{Id: id}, err
}

func RegisterStudent(db *gorm.DB, req *Request) (*Response, error) {
	newStudent := &Student{
		FirstName:  req.FirstName,
		LastName:   req.LastName,
		Studentuid: req.StudentID,
	}

	id, err := CreateStudent(db, newStudent)
	if err != nil {
		return nil, err
	}
	return &Response{Id: id}, err
}
