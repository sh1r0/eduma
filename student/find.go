package student

import "github.com/jinzhu/gorm"

func FindParentByEmail(db *gorm.DB, email string) (*Parent, error) {
	var parent Parent
	res := db.Find(&parent, &Parent{Email: email})
	if res.RecordNotFound() {
		return nil, &ParentNotExistsError{}
	}
	return &parent, nil
}

func FindStudentByID(db *gorm.DB, uid string) (*Student, error) {
	var student Student
	res := db.Find(&student, &Student{Studentuid: uid})
	if res.RecordNotFound() {
		return nil, &StudentNotExistsError{}
	}
	return &student, nil
}

func GetAllParents(db *gorm.DB) []*Parent {
	var parents []*Parent
	_ = db.Find(&parents)
	return parents
}
