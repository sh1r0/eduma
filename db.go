package main

import (
	"eduman/student"
	"eduman/teacher"

	"github.com/jinzhu/gorm"
)

func dbMigrate(db *gorm.DB) error {
	return db.AutoMigrate(
		&student.Student{},
		&student.Class{},
		&student.Record{},
		&student.Billing{},
		&student.Parent{},
		&student.Medical{},
		&student.Clinic{},
		&teacher.Teacher{},
	).Error
}
