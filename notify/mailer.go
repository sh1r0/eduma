package notify

import (
	"bytes"
	"fmt"
	"html/template"
	"log"
	"net/smtp"
	"time"
)

type Request struct {
	from    string
	to      []string
	subject string
	body    string
}

const (
	MIME = "MIME-version: 1.0;\nContent-Type: text/html; charset=\"UTF-8\";\n\n"
)

func NewRequest(to []string, subject string) *Request {
	return &Request{
		to:      to,
		subject: subject,
	}
}

func (r *Request) parseTemplate(fileName string, data interface{}) error {
	t, err := template.ParseFiles(fileName)
	if err != nil {
		return err
	}
	buffer := new(bytes.Buffer)
	if err = t.Execute(buffer, data); err != nil {
		return err
	}
	r.body = buffer.String()
	return nil
}

func (r *Request) sendMail() bool {
	date := time.Now()
	body := "From: " + config.Email + "\r\nDate: " + date.Format("2006-01-02 15:04:05") + "\r\nTo: " + r.to[0] + "\r\nSubject: " + r.subject + "\r\n" + MIME + "\r\n" + r.body
	SMTP := fmt.Sprintf("%s:%d", config.Server, config.Port)
	if err := smtp.SendMail(SMTP, smtp.PlainAuth("", config.Email, config.Password, config.Server), config.Email, r.to, []byte(body)); err != nil {
		return false
	}
	return true
}

func (r *Request) Send(templateName string, items interface{}) error {
	err := r.parseTemplate(templateName, items)
	if err != nil {
		log.Fatal(err)
	}
	if ok := r.sendMail(); ok {
		// log.Printf("Email has been sent to %s\n", r.to)
		return nil
	} else {
		// log.Printf("Failed to send the email to %s\n", r.to)
		return &SendingFailedError{}
	}
}

type SendingFailedError struct{}

func (*SendingFailedError) Error() string {
	return "Failed to send the email."
}
