package notify

var config = Config{}

func init() {
	config.Read()
}

func SignupNotification(username string, email string) {
	subject := "Thank you for joining EduMan community"
	destination := email
	r := NewRequest([]string{destination}, subject)
	_ = r.Send("templates/mail/signup.html", map[string]string{"username": username})
}

func NewSubscriber(name string, email string) {
	subject := "Thank you for subscribing to EduMan"
	destination := email
	r := NewRequest([]string{destination}, subject)
	_ = r.Send("templates/mail/newsubscriber.html", map[string]string{"name": name})
}

func SendNewEmail(email string, subject string, message string) {
	req := &Request{
		to:      []string{email},
		subject: subject,
		body:    message,
	}
	_ = req.sendMail()
}
