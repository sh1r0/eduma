package notify

import "github.com/jinzhu/gorm"

type Notification struct {
	gorm.Model
}

type ContactMessage struct {
	gorm.Model
	Name    string
	Email   string
	Message string
}
