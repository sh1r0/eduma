package notify

import (
	"log"
	"megacog/utils"

	"github.com/BurntSushi/toml"
)

type Config struct {
	Server   string
	Port     int
	Email    string
	Password string
}

func (c *Config) Read() {
	configFilePath := utils.AppFilePath("notify/config.toml")
	if _, err := toml.DecodeFile(configFilePath, &c); err != nil {
		log.Fatal(err)
	}
}
